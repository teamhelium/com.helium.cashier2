<?php

namespace Helium\Cashier2;

use Helium\Cashier2\Exceptions\InvalidBankAccount;
use Stripe\BankAccount as StripeBankAccount;

class BankAccount
{
    /**
     * The Stripe model instance.
     *
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $owner;

    /**
     * The Stripe BankAccount instance.
     *
     * @var \Stripe\BankAccount
     */
    protected $bankAccount;

    /**
     * Create a new BankAccount instance.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $owner
     * @param  \Stripe\BankAccount  $bankAccount
     * @return void
     */
    public function __construct($owner, StripeBankAccount $bankAccount)
    {
        if ($owner->stripe_id !== $bankAccount->customer) {
            throw InvalidBankAccount::invalidOwner($bankAccount, $owner);
        }

        $this->owner = $owner;
        $this->bankAccount = $bankAccount;
    }

    /**
     * Verify the bank account.
     *
     * @param int $deposit1
     * @param int $deposit2
     * @return void
     */
    public function verify(int $deposit1, int $deposit2)
    {
        $this->bankAccount->verify([
            'amounts' => [$deposit1, $deposit2]
        ]);
    }

    /**
     * Delete the payment method.
     *
     * @return \Stripe\BankAccount
     */
    public function delete()
    {
        return $this->owner->removePaymentMethod($this->bankAccount);
    }

    /**
     * Get the Stripe model instance.
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function owner()
    {
        return $this->owner;
    }

    public function isVerified()
    {
        return $this->bankAccount->status == StripeBankAccount::STATUS_VERIFIED;
    }

    public function isUnverified()
    {
        return !$this->isVerified();
    }

    /**
     * Get the Stripe BankAccount instance.
     *
     * @return \Stripe\BankAccount
     */
    public function asStripePaymentMethod()
    {
        return $this->asStripeBankAccount();
    }

    /**
     * Get the Stripe BankAccount instance.
     *
     * @return \Stripe\BankAccount
     */
    public function asStripeBankAccount()
    {
        return $this->bankAccount;
    }

    /**
     * Dynamically get values from the Stripe PaymentMethod.
     *
     * @param  string  $key
     * @return mixed
     */
    public function __get($key)
    {
        return $this->bankAccount->{$key};
    }
}