<?php

namespace Helium\Cashier2;

use Helium\Cashier2\Concerns\ManagesPaymentMethods2;
use Helium\Cashier2\Concerns\ManagesSubscriptions2;
use Helium\Cashier2\Concerns\PerformsCharges2;

trait Billable2
{
    use ManagesPaymentMethods2;
    use PerformsCharges2;
    use ManagesSubscriptions2;
}