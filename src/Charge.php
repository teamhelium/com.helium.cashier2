<?php

namespace Helium\Cashier2;

use Helium\Cashier2\Exceptions\ChargeFailure;
use Laravel\Cashier\Cashier;
use Stripe\Charge as StripeCharge;

class Charge
{
    /**
     * The Stripe Charge instance.
     *
     * @var \Stripe\Charge
     */
    protected $charge;

    /**
     * Create a new Charge instance.
     *
     * @param  \Stripe\Charge  $charge
     * @return void
     */
    public function __construct(StripeCharge $charge)
    {
        $this->charge = $charge;
    }

    /**
     * Get the total amount that will be paid.
     *
     * @return string
     */
    public function amount()
    {
        return Cashier::formatAmount($this->rawAmount(), $this->charge->currency);
    }

    /**
     * Get the raw total amount that will be paid.
     *
     * @return int
     */
    public function rawAmount()
    {
        return $this->charge->amount;
    }

    /**
     * Determine if the payment failed.
     *
     * @return bool
     */
    public function isFailed()
    {
        return $this->charge->status === StripeCharge::STATUS_FAILED;
    }

    /**
     * Determine if the payment was successful.
     *
     * @return bool
     */
    public function isSucceeded()
    {
        return $this->charge->status === StripeCharge::STATUS_SUCCEEDED;
    }

    /**
     * Validate if the charge was successful and throw an exception if not.
     *
     * @return void
     *
     * @throws
     */
    public function validate()
    {
        if ($this->isFailed()) {
            throw ChargeFailure::chargeFailed($this);
        }
    }

    /**
     * The Stripe Charge instance.
     *
     * @return \Stripe\Charge
     */
    public function asStripeCharge()
    {
        return $this->charge;
    }

    /**
     * Dynamically get values from the Stripe Charge.
     *
     * @param  string  $key
     * @return mixed
     */
    public function __get($key)
    {
        return $this->charge->{$key};
    }
}