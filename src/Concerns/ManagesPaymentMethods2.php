<?php

namespace Helium\Cashier2\Concerns;

use Helium\Cashier2\BankAccount;
use Helium\Cashier2\Events\DefaultPaymentMethodUpdated;
use Helium\Cashier2\Notifications\BankAccountVerificationNotification;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Laravel\Cashier\Billable;
use Laravel\Cashier\PaymentMethod;
use Stripe\BankAccount as StripeBankAccount;
use Stripe\Customer;
use Stripe\Customer as StripeCustomer;
use Stripe\Exception\ApiErrorException as StripeApiErrorException;
use Stripe\PaymentMethod as StripePaymentMethod;

/**
 * @mixin Model
 * @mixin Billable
 */
trait ManagesPaymentMethods2
{
    /**
     * Get a collection of the entity's payment methods.
     *
     * @param  array  $parameters
     * @return \Illuminate\Support\Collection|\Laravel\Cashier\PaymentMethod[]
     */
    public function paymentMethods($parameters = [])
    {
        if (! $this->hasStripeId()) {
            return collect();
        }

        $parameters = array_merge(['limit' => 24], $parameters);

        // "type" is temporarily required by Stripe...
        $paymentMethods = StripePaymentMethod::all(
            ['customer' => $this->stripe_id, 'type' => 'card'] + $parameters,
            $this->stripeOptions()
        );
        $bankAccounts = Customer::allSources(
            $this->stripeId(),
            ['object' => 'bank_account']  + $parameters,
            $this->stripeOptions()
        );

        return collect($paymentMethods->data)->map(function ($paymentMethod) {
            return new PaymentMethod($this, $paymentMethod);
        })->merge(collect($bankAccounts->data)->map(function ($bankAccount) {
            return new BankAccount($this, $bankAccount);
        }));
    }

    /**
     * Add a payment method to the customer.
     *
     * @param  \Stripe\PaymentMethod|string  $paymentMethod
     * @return \Laravel\Cashier\PaymentMethod|\Helium\Cashier2\BankAccount
     */
    public function addPaymentMethod($paymentMethod)
    {
        $this->assertCustomerExists();

        $stripePaymentMethod = $this->resolveStripePaymentMethod($paymentMethod);

        if ($stripePaymentMethod->customer !== $this->stripe_id) {
            $stripePaymentMethod = $stripePaymentMethod->attach(
                ['customer' => $this->stripe_id], $this->stripeOptions()
            );
        }

        if ($stripePaymentMethod instanceof StripeBankAccount) {
            return new BankAccount($this, $stripePaymentMethod);
        }

        return new PaymentMethod($this, $stripePaymentMethod);
    }

    /**
     * Send email explaining how to verify Bank Account.
     */
    public function sendBankAccountVerificationNotification()
    {
        $this->notify(new BankAccountVerificationNotification());
    }

    /**
     * Verify the bank account.
     *
     * @param \Helium\Cashier2\BankAccount|\Stripe\BankAccount $bankAccount
     * @param int $deposit1
     * @param int $deposit2
     */
    public function verifyBankAccount($bankAccount, int $deposit1, int $deposit2)
    {
        $bankAccount = $this->resolveStripePaymentMethod($bankAccount);

        if ($bankAccount instanceof StripeBankAccount) {
            $bankAccount = new BankAccount($this, $bankAccount);
        }

        $bankAccount->verify($deposit1, $deposit2);

        $this->updateDefaultPaymentMethodFromStripe();

        return $bankAccount;
    }

    /**
     * Remove a payment method from the customer.
     *
     * @param  \Stripe\PaymentMethod|\Stripe\BankAccount|string  $paymentMethod
     * @return void
     */
    public function removePaymentMethod($paymentMethod)
    {
        $this->assertCustomerExists();

        $stripePaymentMethod = $this->resolveStripePaymentMethod($paymentMethod);

        if ($stripePaymentMethod->customer !== $this->stripe_id) {
            return;
        }

        if ($stripePaymentMethod instanceof StripePaymentMethod) {
            $stripePaymentMethod->detach(null, $this->stripeOptions());
        } elseif ($stripePaymentMethod instanceof StripeBankAccount) {
            $stripePaymentMethod->delete(null, $this->stripeOptions());
        }

        $this->updateDefaultPaymentMethodFromStripe();
    }

    /**
     * Get the default payment method for the entity.
     *
     * @return \Laravel\Cashier\PaymentMethod|\Helium\Cashier2\BankAccount|\Stripe\Card|\Stripe\BankAccount|null
     */
    public function defaultPaymentMethod()
    {
        if (! $this->hasStripeId()) {
            return;
        }

        $customer = StripeCustomer::retrieve([
            'id' => $this->stripe_id,
            'expand' => [
                'invoice_settings.default_payment_method',
                'default_source',
            ],
        ], $this->stripeOptions());

        if ($customer->invoice_settings->default_payment_method) {
            return new PaymentMethod($this, $customer->invoice_settings->default_payment_method);
        }

        // If we can't find a payment method, try to return a legacy source...
        if ($customer->default_source instanceof StripeBankAccount) {
            return new BankAccount($this, $customer->default_source);
        };
    }

    /**
     * Update customer's default payment method.
     *
     * @param  \Stripe\PaymentMethod|\Stripe\BankAccount|string  $paymentMethod
     * @return \Laravel\Cashier\PaymentMethod|\Helium\Cashier2\BankAccount
     */
    public function updateDefaultPaymentMethod($paymentMethod)
    {
        $this->assertCustomerExists();

        if (!$paymentMethod) {
            $this->deletePaymentMethods();
            return null;
        }

        $customer = $this->asStripeCustomer();

        $stripePaymentMethod = $this->resolveStripePaymentMethod($paymentMethod);

        // If the customer already has the payment method as their default, we can bail out
        // of the call now. We don't need to keep adding the same payment method to this
        // model's account every single time we go through this specific process call.
        $defaultPaymentMethod = $this->defaultPaymentMethod();

        if ($defaultPaymentMethod && $stripePaymentMethod->id === $defaultPaymentMethod->id) {
            return $defaultPaymentMethod;
        }

        $paymentMethod = $this->addPaymentMethod($stripePaymentMethod);

        if ($paymentMethod instanceof BankAccount)
        {
            $customer->invoice_settings->default_payment_method = null;
            $customer->default_source = $paymentMethod->id;
        }
        else
        {
            $customer->invoice_settings = ['default_payment_method' => $paymentMethod->id];
        }

        $customer->save($this->stripeOptions());

        // Next we will get the default payment method for this user so we can update the
        // payment method details on the record in the database. This will allow us to
        // show that information on the front-end when updating the payment methods.
        $this->fillPaymentMethodDetails($paymentMethod);

        $this->save();

        event(new DefaultPaymentMethodUpdated($this, $paymentMethod));

        return $paymentMethod;
    }

    /**
     * Synchronises the customer's default payment method from Stripe back into the database.
     *
     * @return $this
     */
    public function updateDefaultPaymentMethodFromStripe()
    {
        $defaultPaymentMethod = $this->defaultPaymentMethod();

        if ($defaultPaymentMethod) {
            if ($defaultPaymentMethod instanceof PaymentMethod) {
                $this->fillPaymentMethodDetails(
                    $defaultPaymentMethod->asStripePaymentMethod()
                )->save();
            } elseif ($defaultPaymentMethod instanceof BankAccount) {
                $this->fillPaymentMethodDetails(
                    $defaultPaymentMethod
                )->save();
            } else {
                $this->fillSourceDetails($defaultPaymentMethod)->save();
            }
        } else {
            $this->forceFill([
                'card_brand' => null,
                'card_last_four' => null,
            ])->save();
        }

        event(new DefaultPaymentMethodUpdated($this, $defaultPaymentMethod));

        return $this;
    }

    /**
     * Fills the model's properties with the payment method from Stripe.
     *
     * @param  \Laravel\Cashier\PaymentMethod|\Helium\Cashier2\BankAccount|\Stripe\PaymentMethod|null  $paymentMethod
     * @return $this
     */
    protected function fillPaymentMethodDetails($paymentMethod)
    {
        if ($paymentMethod instanceof BankAccount) {
            if ($paymentMethod->asStripeBankAccount()->status == 'verified') {
                $this->card_brand = 'Bank Account';
            } else {
                $this->card_brand = 'Bank Account (Pending Verification)';
            }

            $this->card_last_four = $paymentMethod->last4;
        } elseif ($paymentMethod->type === 'card') {
            $this->card_brand = $paymentMethod->card->brand;
            $this->card_last_four = $paymentMethod->card->last4;
        }

        return $this;
    }

    /**
     * Deletes the entity's payment methods.
     *
     * @return void
     */
    public function deletePaymentMethods()
    {
        $this->paymentMethods()->each(function ($paymentMethod) {
            $paymentMethod->delete();
        });

        $this->updateDefaultPaymentMethodFromStripe();
    }

    /**
     * Resolve a PaymentMethod ID to a Stripe PaymentMethod object.
     *
     * @param  \Stripe\PaymentMethod|string  $paymentMethod
     * @return \Stripe\PaymentMethod|\Stripe\BankAccount
     */
    protected function resolveStripePaymentMethod($paymentMethod)
    {
        if ($paymentMethod instanceof StripePaymentMethod || $paymentMethod instanceof StripeBankAccount) {
            return $paymentMethod;
        }

        if (is_string($paymentMethod) && Str::of($paymentMethod)->startsWith('btok'))
        {
            $bankAccount = StripeCustomer::createSource(
                $this->stripeId(),
                ['source' => $paymentMethod]
            );

            $this->updateDefaultPaymentMethodFromStripe();

            return $bankAccount;
        }
        try {
            return StripeCustomer::retrieveSource(
                $this->stripeId(),
                $paymentMethod,
                null,
                $this->stripeOptions()
            );
        } catch (StripeApiErrorException $e) {
            return StripePaymentMethod::retrieve(
                $paymentMethod, $this->stripeOptions()
            );
        }
    }
}