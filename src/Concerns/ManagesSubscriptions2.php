<?php

namespace Helium\Cashier2\Concerns;

use Helium\Cashier2\SubscriptionBuilder2;

trait ManagesSubscriptions2
{
    /**
     * Begin creating a new subscription.
     *
     * @param  string  $name
     * @param  string|string[]  $plans
     * @return \Helium\Cashier2\SubscriptionBuilder2
     */
    public function newSubscription($name, $plans)
    {
        return new SubscriptionBuilder2($this, $name, $plans);
    }
}