<?php

namespace Helium\Cashier2\Concerns;

use Helium\Cashier2\Charge;
use Illuminate\Support\Str;
use Laravel\Cashier\Payment;
use Stripe\PaymentIntent as StripePaymentIntent;
use Stripe\Refund as StripeRefund;
use Stripe\Charge as StripeCharge;

trait PerformsCharges2
{
    /**
     * Make a "one off" charge on the customer for the given amount.
     *
     * @param  int  $amount
     * @param  string  $paymentMethod
     * @param  array  $options
     * @return \Laravel\Cashier\Payment|\Helium\Cashier2\Charge
     *
     * @throws \Laravel\Cashier\Exceptions\PaymentActionRequired
     * @throws \Laravel\Cashier\Exceptions\PaymentFailure
     * @throws \Helium\Cashier2\Exceptions\ChargeFailure
     */
    public function charge($amount, $paymentMethod, array $options = [])
    {
        if (Str::of($paymentMethod)->startsWith('ba')) {
            $options = array_merge($options, [
                'amount' => $amount,
                'currency' => $this->preferredCurrency(),
                'source' => $paymentMethod
            ]);

            if ($this->hasStripeId()) {
                $options['customer'] = $this->stripe_id;
            }

            $charge = new Charge(
                StripeCharge::create($options, $this->stripeOptions())
            );

            $charge->validate();

            return $charge;

        } else {
            $options = array_merge($options, [
                'confirmation_method' => 'automatic',
                'confirm' => true,
                'currency' => $this->preferredCurrency(),
                'amount' => $amount,
                'payment_method' => $paymentMethod
            ]);

            if ($this->hasStripeId()) {
                $options['customer'] = $this->stripe_id;
            }

            $payment = new Payment(
                StripePaymentIntent::create($options, $this->stripeOptions())
            );

            $payment->validate();

            return $payment;
        }
    }

    /**
     * Refund a customer for a charge.
     *
     * @param  string  $paymentIntent
     * @param  array  $options
     * @return \Stripe\Refund
     */
    public function refund($paymentIntent, array $options = [])
    {
        if (Str::of($paymentIntent)->startsWith('ch')) {
            $params = ['charge' => $paymentIntent];
        } else {
            $params = ['payment_intent' => $paymentIntent];
        }

        return StripeRefund::create(
            $params + $options,
            $this->stripeOptions()
        );
    }
}