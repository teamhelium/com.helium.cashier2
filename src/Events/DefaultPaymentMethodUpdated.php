<?php

namespace Helium\Cashier2\Events;

class DefaultPaymentMethodUpdated
{
    public $billable;

    public $paymentMethod;

    public function __construct($billable, $paymentMethod)
    {
        $this->billable = $billable;
        $this->paymentMethod = $paymentMethod;
    }
}