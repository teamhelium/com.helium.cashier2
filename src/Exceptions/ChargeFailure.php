<?php

namespace Helium\Cashier2\Exceptions;

use Helium\Cashier2\Charge;
use Throwable;

class ChargeFailure extends \Exception
{
    /**
     * The Cashier2 Charge object.
     *
     * @var \Helium\Cashier2\Charge
     */
    public $charge;

    /**
     * Create a new IncompletePayment instance.
     *
     * @param  \Helium\Cashier2\Charge  $charge
     * @param  string  $message
     * @param  int  $code
     * @param  \Throwable|null  $previous
     * @return void
     */
    public function __construct(Charge $charge, $message = '', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->charge = $charge;
    }

    /**
     * Create a new ChargeFailure instance.
     *
     * @param  \Helium\Cashier2\Charge  $charge
     * @return static
     */
    public static function chargeFailed(Charge $charge)
    {
        return new static(
            $charge,
            'The payment failed because of an invalid payment method or insufficient funds.'
        );
    }
}