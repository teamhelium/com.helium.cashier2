<?php

namespace Helium\Cashier2\Exceptions;

use Stripe\BankAccount as StripeBankAccount;

class InvalidBankAccount extends \Exception
{
    /**
     * Create a new InvalidPaymentMethod instance.
     *
     * @param  \Stripe\BankAccount  $bankAccount
     * @param  \Illuminate\Database\Eloquent\Model  $owner
     * @return static
     */
    public static function invalidOwner(StripeBankAccount $bankAccount, $owner)
    {
        return new static(
            "The bank account `{$bankAccount->id}` does not belong to this customer `$owner->stripe_id`."
        );
    }
}