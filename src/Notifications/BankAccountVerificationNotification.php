<?php

namespace Helium\Cashier2\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class BankAccountVerificationNotification extends Notification
{
    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject(__('Verify Your Bank Account'))
            ->greeting(__('Your Bank Account Requires Verification'))
            ->line(__('You must verify your bank account before you can make a payment.'))
            ->line(__('You will receive two small deposits and one withdrawal on your bank account.'))
            ->line(__('Make note of the deposit amounts, then log and verify your bank account on your payment settings.'))
            ->line(__('Please note that it may take 1 - 2 Business days for the transactions to appear on your account.'));
    }
}