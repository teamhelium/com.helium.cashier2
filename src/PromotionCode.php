<?php

namespace Helium\Cashier2;

use Stripe\PromotionCode as StripePromotionCode;

class PromotionCode
{
    /**
     * The Stripe PromotionCode instance.
     *
     * @var StripePromotionCode
     */
    protected $promotionCode;

    /**
     * Create a new PromotionCode instance.
     *
     * @param StripePromotionCode $promotionCode
     */
    public function __construct(StripePromotionCode $promotionCode)
    {
        $this->promotionCode = $promotionCode;
    }

    /**
     * Determine if the PromotionCode can be redeemed.
     *
     * @param int $transactionAmount Amount of transaction in cents
     * @param bool $firstTimeTransaction First time transaction
     * @param string|null $customer
     * @return bool
     */
    public function canBeRedeemed(int $transactionAmount, bool $firstTimeTransaction, string $customer = null)
    {
        return $this->promotionCode->coupon->valid
            && $this->promotionCode->expires_at < time()
            && $this->promotionCode->coupon->redeem_by < time()
            && (!$this->promotionCode->restrictions->first_time_transaction || $firstTimeTransaction)
            && $transactionAmount > ($this->promotionCode->minimum_amount ?? -1)
            && (is_null($this->promotionCode->customer) || $this->promotionCode->customer == $customer)
            && (is_null($this->promotionCode->max_redemptions) || $this->promotionCode->times_redeemed < $this->promotionCode->max_redemptions)
            && (is_null($this->promotionCode->coupon->max_redemptions) || $this->promotionCode->coupon->times_redeemed < $this->promotionCode->coupon->max_redemptions);
    }

    /**
     * Get the Stripe PromotionCode instance.
     *
     * @return StripePromotionCode
     */
    public function asStripePromotionCode()
    {
        return $this->promotionCode;
    }

    /**
     * Dynamically get values from the Stripe PromotionCode.
     *
     * @param  string  $key
     * @return mixed
     */
    public function __get($key)
    {
        return $this->promotionCode->{$key};
    }
}