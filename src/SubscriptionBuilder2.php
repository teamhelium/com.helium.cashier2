<?php

namespace Helium\Cashier2;

use Laravel\Cashier\Payment;
use Laravel\Cashier\SubscriptionBuilder;
use Stripe\Subscription as StripeSubscription;

class SubscriptionBuilder2 extends SubscriptionBuilder
{
    /**
     * Create a new Stripe subscription.
     *
     * @param  \Stripe\PaymentMethod|\Stripe\BankAccount|string|null  $paymentMethod
     * @param  array  $customerOptions
     * @param  array  $subscriptionOptions
     * @return \Laravel\Cashier\Subscription
     *
     * @throws \Laravel\Cashier\Exceptions\PaymentActionRequired
     * @throws \Laravel\Cashier\Exceptions\PaymentFailure
     */
    public function create($paymentMethod = null, array $customerOptions = [], array $subscriptionOptions = [])
    {
        $customer = $this->getStripeCustomer($paymentMethod, $customerOptions);

        $params = ['customer' => $customer->id];

        if ($this->owner->defaultPaymentMethod() instanceof BankAccount) {
            $params['payment_behavior'] = StripeSubscription::PAYMENT_BEHAVIOR_ALLOW_INCOMPLETE;
        }

        $payload = array_merge(
            $params,
            $this->buildPayload(),
            $subscriptionOptions
        );

        $stripeSubscription = StripeSubscription::create(
            $payload,
            $this->owner->stripeOptions()
        );

        if ($this->skipTrial) {
            $trialEndsAt = null;
        } else {
            $trialEndsAt = $this->trialExpires;
        }

        /** @var \Laravel\Cashier\Subscription $subscription */
        $subscription = $this->owner->subscriptions()->create([
            'name' => $this->name,
            'stripe_id' => $stripeSubscription->id,
            'stripe_status' => $stripeSubscription->status,
            'stripe_plan' => $stripeSubscription->plan ? $stripeSubscription->plan->id : null,
            'quantity' => $stripeSubscription->quantity,
            'trial_ends_at' => $trialEndsAt,
            'ends_at' => null,
        ]);

        /** @var \Stripe\SubscriptionItem $item */
        foreach ($stripeSubscription->items as $item) {
            $subscription->items()->create([
                'stripe_id' => $item->id,
                'stripe_plan' => $item->plan->id,
                'quantity' => $item->quantity,
            ]);
        }

        if ($subscription->hasIncompletePayment()) {
            if ($stripeSubscription->latest_invoice->charge) {
                (new Charge(
                    $stripeSubscription->latest_invoice->charge
                ))->validate();
            } else {
                (new Payment(
                    $stripeSubscription->latest_invoice->payment_intent
                ))->validate();
            }
        }

        return $subscription;
    }

    /**
     * Get the Stripe customer instance for the current user and payment method.
     *
     * @param  \Stripe\PaymentMethod|\Stripe\BankAccount|string|null  $paymentMethod
     * @param  array  $options
     * @return \Stripe\Customer
     */
    protected function getStripeCustomer($paymentMethod = null, array $options = [])
    {
        return parent::getStripeCustomer($paymentMethod, $options);
    }

    /**
     * Build the payload for subscription creation.
     *
     * @return array
     */
    protected function buildPayload()
    {
        $payload = parent::buildPayload();
        $expand = $payload['expand'] ?? [];
        $payload['expand'] = array_merge($expand, [
            'latest_invoice.charge'
        ]);

        return $payload;
    }
}